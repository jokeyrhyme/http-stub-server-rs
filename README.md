# http-stub-server-rs

fake web service for testing purposes

## usage

```
$ # after cloning this repository...
$ cargo run --
```

## roadmap

### housekeeping

- [ ] tagging a release triggers `cargo publish`
- [ ] expose a library for use in other projects
- [x] `--version` flag
- [ ] Dockerfile
- [ ] [Cloud Native Application Bundle](https://cnab.io/)

### web server

- [x] log requests to stdout
- [x] set `APP_PORT=1234` environment variable to customise port (default = 8080)
- [x] accept traffic on local interfaces by default
- [x] set `APP_EXTERNAL=true` to accept traffic on all interfaces (default = false)
- [ ] [OpenAPI](https://www.openapis.org/) schema
- [ ] [OpenTelemetry](https://opentelemetry.io/) compatibility for tracing

### HTTP API: `/v1`

- [ ] specify response status code
- [x] `?delay_ms=1234` query string argument to specify response time
- [x] response body based upon `Accept` request header
- [ ] specify response content length
- [ ] ...
- [ ] convenient helpers for testing [GraphQL](https://graphql.org/) ?

### WebSocket API

- [ ] ...
- [ ] helpers for testing [Web Application Messaging Protocol](https://wamp-proto.org/) ?
