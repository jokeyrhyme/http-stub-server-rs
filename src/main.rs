#![deny(clippy::all)]
use std::env;

#[macro_use]
extern crate serde_derive;

use log::LevelFilter;
use log4rs::{
    append::console::ConsoleAppender,
    config::{Appender, Config, Root},
    encode::json::JsonEncoder,
};
use warp::Filter;

mod settings;
mod v1;

use settings::Settings;

#[tokio::main]
async fn main() {
    for arg in env::args_os() {
        match arg.to_string_lossy().as_ref() {
            "-v" | "--version" => {
                println!(env!("CARGO_PKG_VERSION"));
                return;
            }
            _ => { /* noop */ }
        }
    }

    let settings = Settings::new().expect("unable to determine settings");

    let log_json = JsonEncoder::new();
    let log_stdout = ConsoleAppender::builder()
        .encoder(Box::new(log_json))
        .build();
    let log_config = Config::builder()
        .appender(Appender::builder().build("stdout", Box::new(log_stdout)))
        .build(Root::builder().appender("stdout").build(LevelFilter::Info))
        .expect("unable to configure logging");
    log4rs::init_config(log_config).expect("unable to initialise logger");

    let address = (
        if settings.external {
            [0, 0, 0, 0]
        } else {
            [127, 0, 0, 1]
        },
        settings.port.get(),
    );
    warp::serve(v1::filter().with(warp::log("request")))
        .run(address)
        .await;
}
