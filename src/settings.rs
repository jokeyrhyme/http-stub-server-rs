use std::num;

use config::{Config, ConfigError, Environment};

#[derive(Debug, Deserialize)]
#[serde(default)]
pub(crate) struct Settings {
    pub external: bool,
    pub port: num::NonZeroU16,
}
impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let mut s = Config::default();

        s.merge(Environment::with_prefix("app"))?;

        s.try_into()
    }
}
impl Default for Settings {
    fn default() -> Self {
        Self {
            external: false,
            port: num::NonZeroU16::new(8080)
                .expect("cannot convert default port to positive 16-bit number"),
        }
    }
}
