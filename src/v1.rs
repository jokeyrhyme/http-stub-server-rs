use std::{thread, time::Duration};

use warp::{
    filters::BoxedFilter,
    http::{Response, StatusCode},
    reject::Reject,
    Filter,
};

pub(crate) fn filter() -> BoxedFilter<(impl warp::Reply,)> {
    warp::path!("v1")
        .and(warp::filters::header::optional("accept"))
        .and(warp::filters::query::query())
        .map(handler)
        .boxed()
}

#[derive(Debug)]
struct AcceptedTypeNotImplemented;
impl Reject for AcceptedTypeNotImplemented {}

#[derive(Deserialize)]
struct QueryStringParameters {
    delay_ms: Option<u64>,
}

fn handler(accept: Option<String>, qsa: QueryStringParameters) -> impl warp::Reply {
    let mut accept = accept.unwrap_or_else(|| String::from("text/plain"));

    if let Some(delay_ms) = qsa.delay_ms {
        if delay_ms > 0 {
            thread::sleep(Duration::from_millis(delay_ms));
        }
    }

    if accept.as_str() == "*/*" {
        accept = String::from("text/plain");
    }
    let body = match accept.as_str() {
        "application/json" => r#"{"hello": "world!"}"#,
        "text/plain" => "Hello, world!",
        _ => {
            return Response::builder()
                .header("content-type", "text/plain")
                .status(StatusCode::BAD_REQUEST)
                .body(format!(
                    "response body for 'accept: {}' not implemented",
                    accept
                ));
        }
    };
    Response::builder()
        .header("content-type", accept)
        .body(String::from(body))
}

#[cfg(test)]
mod tests {
    use warp::Reply;

    use std::time::Instant;

    use super::*;

    #[tokio::test]
    async fn test_accept_application_json() {
        let filter = &filter();

        let got = warp::test::request()
            .header("accept", "application/json")
            .path("/v1")
            .filter(filter)
            .await
            .unwrap()
            .into_response();
        assert_eq!(got.status(), StatusCode::OK);
        assert_eq!(
            got.headers()
                .get("content-type")
                .expect("no content-type header"),
            "application/json"
        );

        let got_body = warp::hyper::body::to_bytes(got.into_body())
            .await
            .expect("cannot read response bytes");
        assert_eq!(got_body, r#"{"hello": "world!"}"#);
    }

    #[tokio::test]
    async fn test_delay_ms_none_is_fast_enough() {
        let filter = &filter();
        let start = Instant::now();

        let got = warp::test::request()
            .path("/v1")
            .filter(filter)
            .await
            .unwrap()
            .into_response();
        assert_eq!(got.status(), StatusCode::OK);

        let duration = start.elapsed();
        assert!(duration < Duration::from_millis(100));
    }

    #[tokio::test]
    async fn test_delay_ms_3_seconds() {
        let filter = &filter();
        let start = Instant::now();

        let got = warp::test::request()
            .path("/v1?delay_ms=3000")
            .filter(filter)
            .await
            .unwrap()
            .into_response();
        assert_eq!(got.status(), StatusCode::OK);

        let duration = start.elapsed();
        assert!(duration >= Duration::from_millis(3000));
    }
}
